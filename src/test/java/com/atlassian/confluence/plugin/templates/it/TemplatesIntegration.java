package com.atlassian.confluence.plugin.templates.it;

import com.atlassian.confluence.plugin.functest.helper.SpaceHelper;

/**
 * @author Ryan Thomas
 */
public class TemplatesIntegration extends AbstractIntegration
{
    private static final String TEST_SPACE_KEY = "TEST" + String.valueOf((int) (Math.random() * 10000.0));
    private static final String SPACE_NAME = "Test templates space (" + TEST_SPACE_KEY + ")";

    protected void tearDown() {}

    private void createSpace() {
        SpaceHelper helper = getSpaceHelper();
        helper.setName(SPACE_NAME);
        helper.setKey(TEST_SPACE_KEY);
        helper.create();
    }

    private void deleteSpace() {
        getSpaceHelper(TEST_SPACE_KEY).delete();
    }

    public void testMenuItemExists() {
        gotoPage("/admin/console.action");
        assertLinkPresentWithExactText("Import Templates");
    }

    public void testDefaultsAvailable() {
        gotoPage("/admin/plugins/templatePackage/configurePlugin.action");
        assertTextPresent("Default Templates Package (14)");
    }

    public void testNoneSelectedImport() {
        createSpace();
        gotoPage("/admin/plugins/templatePackage/configurePlugin.action");
        assertTextPresent("Default Templates Package (14)");

        selectOption("spaceKey", SPACE_NAME);
        submit("", "Import");
        assertTextPresent("You must specify at least one template to import and a destination space.");
        deleteSpace();
    }

    public void testImportTemplate() {
        createSpace();
        gotoPage("/admin/plugins/templatePackage/configurePlugin.action");
        assertTextPresent("Default Templates Package (14)");

        clickElementByXPath("//input[@type='checkbox' and @name='toInstall']");

        selectOption("spaceKey", SPACE_NAME);
        submit("", "Import");
        assertTextPresent("1 Templates Imported to: " + SPACE_NAME);

        gotoPage("/pages/templates2/listpagetemplates.action?key=" + TEST_SPACE_KEY);

        assertTextPresent("Atlassian Support Group Meeting");
        deleteSpace();
    }

    public void testNoTemplatesAvailable() {
        gotoPage("/admin/viewplugins.action?pluginKey=com.atlassian.confluence.plugin.templates.packages.DefaultTemplatesPackage");
        clickLinkWithExactText("Disable plugin");
        gotoPage("/admin/plugins/templatePackage/configurePlugin.action");
        assertTextPresent("There are currently no templates available.");
        gotoPage("/admin/viewplugins.action?pluginKey=com.atlassian.confluence.plugin.templates.packages.DefaultTemplatesPackage");
        clickLinkWithExactText("Enable plugin");
    }
}
